var pdj = {};

pdj.init = function () {}
pdj.shutdown = function () {}

pdj.selectedLoopSize   = {
  "[Channel1]" : 0,
  "[Channel2]" : 0,
  "[Channel3]" : 0,
  "[Channel4]" : 0
};

var intToLoopSizeMap = {
  0 : "0.03125",
  1 : "0.0625",
  2 : "0.125",
  3 : "0.25",
  4 : "0.5",
  5 : "1",
  6 : "2",
  7 : "4",
  8 : "8",
  9 : "16",
  10 : "32",
  11 : "64"
};

pdj.beatloop_size = function (channel, control, value, status, group) {
    pdj.selectedLoopSize[group] = intToLoopSizeMap[value];
    engine.setValue(group, "beatloop_size", pdj.selectedLoopSize[group]);
}

pdj.beatloop_toggle = function (channel, control, value, status, group) {
    var looping = engine.getValue(group, "loop_enabled");    
    if(looping){
       engine.setValue(group, "reloop_toggle", 1);    
    }else{
       engine.setValue(group, "beatloop_activate", 1);    
     }
}
